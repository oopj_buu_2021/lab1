import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        String grade;
        System.out.print("Please input your score : ");
        Scanner sc = new Scanner(System.in);
        int score = sc.nextInt();
        if (score >= 80) {
            grade = "A";
        } else if (score >= 75 && score < 80) {
            grade = "B+";
        } else if (score >= 70 && score < 75) {
            grade = "B";
        } else if (score >= 65 && score < 70) {
            grade = "C+";
        } else if (score >= 60 && score < 65) {
            grade = "C";
        } else if (score >= 55 && score < 60) {
            grade = "D+";
        } else if (score >= 50 && score < 55) {
            grade = "D";
        } else {
            grade = "F";
        }

        System.out.println("Grade : " + grade);

    }

}
